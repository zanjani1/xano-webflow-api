<script>
// Grab the ID stored in localStorage on the previous page.
const recordId = localStorage.id
// Create a request variable and assign a new XMLHttpRequest object to it.
var request = new XMLHttpRequest()
let xanoUrl = new URL('https://x9qk-rkcz-tbuf.n7.xano.io/api:kK5vNkx-/users/' + recordId);
// Open a new connection, using the GET request on the URL endpoint
request.open('GET', xanoUrl.toString(), true)
request.onload = function() {
    // Begin accessing JSON data here
    var data = JSON.parse(this.response)
    if (request.status >= 200 && request.status < 400) {
        // Map a variable called itemContainer to the Webflow element called "Item-Container"
        const itemContainer = document.getElementById('Item-Container')
        // For each restaurant, create a div called card and style with the "Sample Card" class
        const item = document.getElementById('samplestyle')
        
        const img = item.getElementsByTagName('img')[0]
        img.src = data.photo.url + "?tpl=big:box";
        
        const h1 = item.getElementsByTagName('h1')[0]
        h1.textContent = data.name;
        
        const h3 = item.getElementsByTagName('h3')[0]
        h3.textContent = data.twitter;
        
        const link = item.getElementsByTagName('a')[0]
        link.href = data.link;
        link.textContent = data.link;
        
        const bio = item.getElementsByTagName('p')[0]
        bio.textContent = data.bio;
        // Append the card to the div with "Item-Container" id
        itemContainer.appendChild(item);
    } else {
        console.log('error')
    }
}
// Send request
request.send()
</script>
