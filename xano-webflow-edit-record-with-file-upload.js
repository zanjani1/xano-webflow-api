<script>
var Webflow = Webflow || [];
Webflow.push(function() {  
  // unbind webflow form handling (keep this if you only want to affect specific forms)
  $(document).off('submit');

  /* Any form on the page */
  $('form').submit(function(e) {
    e.preventDefault();

  	const $form = $(this); // The submitted form
    const $submit = $('[type=submit]', $form); // Submit button of form
    const buttonText = $submit.val(); // Original button text
    const buttonWaitingText = $submit.attr('data-wait'); // Waiting button text value
    const formMethod = $form.attr('method'); // Form method (where it submits to)
    const formAction = $form.attr('action'); // Form action (GET/POST)
    const formRedirect = $form.attr('data-redirect'); // Form redirect location
    const xanoID = document.getElementById('id').value // getting the ID of the record to edit
    let requestURL = formAction.concat(xanoID + "?")
    let formData = new FormData;
    
    // Set waiting text
    if (buttonWaitingText) {
      $submit.val(buttonWaitingText); 
    }
    
    $form.serializeArray().forEach(item => {
    	formData.append(item.name, item.value);
    });
    
    let fileEl = document.getElementById('file');
    if (!fileEl || fileEl.files.length != 1) {
    	alert("Please choose a file.");
      return;
    }
    
    formData.append('file', fileEl.files[0])
    
    $.ajax(formAction, {
    	data: formData,
      method: formMethod,
      processData: false,
      contentType: false,
      cache: false
    })
    .done((res) => {
      // If form redirect setting set, then use this and prevent any other actions
      if (formRedirect) { window.location = formRedirect; return; }

    	$form
      	.hide() // optional hiding of form
    		.siblings('.w-form-done').show() // Show success
      	.siblings('.w-form-fail').hide(); // Hide failure
    })
    .fail((res) => {
      $form
      	.siblings('.w-form-done').hide() // Hide success
    	  .siblings('.w-form-fail').show(); // show failure
    })
    .always(() => {
      // Reset text
      $submit.val(buttonText);
    });
  });
});
</script>
